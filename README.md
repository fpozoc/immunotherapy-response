# Immunotherapy response prediction

Statistical analysis of data from several clinical outcomes and immunophenotipic markers aiming to predict immunotherapy response.

Data available in this [Google sheet](https://docs.google.com/spreadsheets/d/18xh2RB6dZnhv4L5zAyroxtVYr5GVMrEO_itAu5qis70/edit#gid=0).

Results has been stored in `results.zip` and can be downloaded [here](https://gitlab.com/fpozoc/immunotherapy-response/-/raw/master/results.zip?inline=false). This file has to be unzipped. Once uncompressed, the project description below is going to show both the path in this folder and the linkable path in this repository.

## Table of Contents:

* [Project Description](#project-description)
* [Data Exploration](#data-exploration)
* [Log Fold Changes analysis](#log-fold-change-analysis)
* [Survival analysis](#survival-analysis)

## Project Description <a class="anchor" id="project-description"></a>

The main objective was looking for immunophenotypic markers associated to response in immunotherapy once data have been processed.

## Data Exploration <a class="anchor" id="data-exploration"></a>

The first objective was to establish a group of study and a group of control patients. Our dataset contains 40 patients treated with immunotherapy (15 responded, and 25 not responded), 39 patients treated with chemotherapy (14 responded, and 25 not responded), and 4 healthy controls.

Patients treated with IT only miss data in the weights cfor 3 patients. Second determination of immunophenotypic markers has a lot of missing values distributed over the data set. There are also missing values in basal determination of immunophenotypic markers of IT patients.

### Pairwise Correlations

The pairwise correlation was stored in [corr_response.csv]('../results/correlations/corr_response.csv').

Variables compared among the rest: 
- `r:response` (categorical).
- `r:progression_free_survival` (continouos).
- `r:overal_survival` (continouos).
- `i:irae` (categorical).

#### Pairplots in immunotherapy patients

A pairplot plots a pairwise relationships in a dataset. We have selected 5 variables to compare what's happening facing with themshelves and the other 4 variables in the comparison.

##### Pairplot (response)

The diagonal plots show that distribution between positive (orange) and negative (blue) response is different in `r:progression_free_survival` and `r:overall_survival` and slightly different in the other features. However, the points are not drawing differences between responders and non responders.

<div align="center">
  <img src="results/correlations/pairplot.png"><br>
</div>

##### Pairplot with Regression line traced

<div align="center">
  <img src="results/correlations/regplot.png"><br>
</div>

### Descriptive statistics

Descriptive statistics was stored in [results/reports/processed_db_profile.all_patients.html](https://gitlab.com/fpozoc/immunotherapy-response/-/tree/master/results/reports) and [results/reports/processed_db_profile.it_patients.html](https://gitlab.com/fpozoc/immunotherapy-response/-/blob/master/results/reports/processed_db_profile.it_patients.html). 

These `html` files can be visualized in a current web browser.

## Log fold change analysis <a class="anchor" id="log-fold-change-analysis"></a>

Fold change is a measure describing how much a quantity changes between an original and a subsequent measurement. 

- First, we have plotted in the y-axis the negative $\log_{10}$ of the p-values (threshold was p-val<0.05 or $-\log_{10}$ p-value>-1.3). Each of the p-values represent a [Mann–Whitney U test](https://en.wikipedia.org/wiki/Mann%E2%80%93Whitney_U_test) between basal and second determination of each immunophenotypic marker. 
- The x-axis represent the difference between the $\log_{2}$ mean of the each of the cases (second determination) and the $\log_{2}$ mean of each of the controls (basal determination). Threshold was set to be higher than 1 or lower than -1.
- Finally, we applied a [Benjamini-Hochberg correction](https://www.statisticshowto.com/benjamini-hochberg-procedure/) to controls the [False Discovery Rate](https://en.wikipedia.org/wiki/False_discovery_rate).

Disclaimer: It is important to assume that in any case our data shows strong differences among any of the variables. We can see some statistically significant differenciated markers using the whole data set. (4.1.1). This differences seems to be higher in bigger datasets (4.1.1, 4.1.2) than lower ones (4.1.3, 4.1.4), therefore we can assume that more data is completely necessary here in order to take decissions based on statistics.

### Basal vs. second determination fold change comparisons

#### Basal vs. second determination comparisons in the whole data set (IT+QT patients)

Table stored in [results/comparisons/all_patients.csv](https://gitlab.com/fpozoc/immunotherapy-response/-/blob/master/results/comparisons/all_patients.csv)

<div align="center">
  <img src="results/comparisons/all_patients.png"><br>
</div>

#### Basal vs. second determination comparisons in IT patients

Table stored in [results/comparisons/IT1.csv](https://gitlab.com/fpozoc/immunotherapy-response/-/blob/master/results/comparisons/IT1.csv)

<div align="center">
  <img src="results/comparisons/IT1.png"><br>
</div>

#### Basal vs. second determination comparisons in responders IT patients

Table stored in [results/comparisons/IT1_R1.csv](https://gitlab.com/fpozoc/immunotherapy-response/-/blob/master/results/comparisons/IT1_R1.csv)

<div align="center">
  <img src="results/comparisons/IT1_R1.png"><br>
</div>

As we have already said above, we can assume statistical differences between response ot IT patients

#### Basal vs. second determination comparisons in non-responders IT patients

Table stored in [results/comparisons/IT1_R0.csv](https://gitlab.com/fpozoc/immunotherapy-response/-/blob/master/results/comparisons/IT1_R0.csv)

<div align="center">
  <img src="results/comparisons/IT1_R0.png"><br>
</div>

### IT responders vs. IT non responders Fold Change analysis

It was an experimental analysis because it is not a classical Fold Change analysis. In any case, the comparisons were not found to be statistically significant.

#### IT responders vs. IT non responders basal determination

Table stored in [results/comparisons/IT1_D1.csv](https://gitlab.com/fpozoc/immunotherapy-response/-/blob/master/results/comparisons/IT1_R0.csv)

<div align="center">
  <img src="results/comparisons/IT1_D1.png"><br>
</div>

#### IT responders vs. IT non responders second determination

Table stored in [results/comparisons/IT1_D2.csv](https://gitlab.com/fpozoc/immunotherapy-response/-/blob/master/results/comparisons/IT1_R0.csv)

<div align="center">
  <img src="results/comparisons/IT1_D2.png"><br>
</div>


## Survival analysis <a class="anchor" id="survival-analysis"></a>

The survival analysis can be explored in [results/survival](https://gitlab.com/fpozoc/immunotherapy-response/-/tree/master/results/survival).