Resultados KM

La anotación de los ficheros sigue este patron, cohorteCaso_vs_cohorteReferencia, el "_vs_" separa los grupos.
Ambas cohortes están codificados con 3 numeros: immuno_response_marker.

immuno = 1: pacientes que reciben inmunoterapia. 
immuno = 0: pacientes que reciben quimioterapia.
response = 1: pacientes que responden al tratamiento.
response = 0: pacientes que no responden al tratamiento.
marker = 1: marcadores primera determinacion.
marker =  2: marcador segunda determinacion.

Ejemplo
Tabla: OS_KM_1_1_2_VS_1_1_1.tsv
Se trata de una tabla con los resultados de Kaplan-Meier (KM) comparando la Overall Survival (OS) de los pacientes tratados con inmunoterapia respondedores (aquellos que tienen determinacion basal y secuendaria). Se hace la diferencia entre el marcados secundario y el basal, se ordena por dicha diferencia y se selecciona aquellos pacientes con mayor aumento del marcador y aquellos pacientes con menos aumento. Estos dos grupos son los que se utilizan para comparar si hay diferendcias entre Overall Survival.

En cada esta  fila indica el marcador testado y un FDR asociado que indica si hay diferencias significativas.

Para encontrar las figuars asociadas a cada test, basta con buscar el pdf que que este codificado de la misma forma.
Ejemplo:
Marcador: CD11c+CD14+_MHCII+_CCR9+ de la tabla OS_KM_1_1_2_VS_1_1_1.tsv
Le corresponde la imagen: Overall_Survival_CD11c+CD14+_MHCII+_CCR9+1_1_2_VS_1_1_1_035.png  

Los grupos son que aparecen en la imagen.
group1: aquellos pacientes que han sufrido un menor aumento o disminucion en la segunda medicion del marcador respecto a la medicion basal
group2: aquellos pacientes que han sufrido un mayor aumento en la segunda medicion del marcador respecto a la medicion basal. 

Atencion 1: En las gráficas viene indicado el p-valor, en las tablas el FDR, el programa usada para generar las graficas no permite poner FDR.
Atencion 2: Se ha usado esta codificacion para evitar que los nombres de los ficheros sean muy largos.

