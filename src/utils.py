#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" utils.py
"""
import re

import pandas as pd 
import numpy as np
from scipy.stats import mannwhitneyu
from collections import defaultdict 
from math import log2, log10
from bioinfokit import visuz
from statsmodels.stats.multitest import multipletests as fdr 
from sklearn import metrics


def generate_scores(model, features, target):
    '''
    It test the model introduced as argument with several common scoring metrics.

    Parameters
    ----------
    model: object
        Scikit-learn object to test.
    features: array
        Features to test.
    target: array
        Target to test.

    Returns
    ----------
    scores: Dict
        Dictionary of scores to test the model with X(features and).
    '''
    predictions = model.predict(features)
    probs = model.predict_proba(features)[:, 1]
    scores = {
        'Accuracy': metrics.accuracy_score(target, predictions),
        'AUC': metrics.roc_auc_score(target, probs),
        'Average Precision Score': metrics.average_precision_score(target, probs),
        'Balanced Accuracy': metrics.balanced_accuracy_score(target, predictions),
        'F1 Score': metrics.f1_score(target, predictions),
        'Log Loss': -1*(metrics.log_loss(target, probs)),
        'Precision': metrics.precision_score(target, predictions),
        'Recall': metrics.recall_score(target, predictions),
        'Classification Report': metrics.classification_report(target, predictions),
        'Confusion Matrix': metrics.confusion_matrix(target, predictions)
        }
    return scores


def get_columns(df, pattern, loc='start'):
    if loc == 'start':
        return list(df[[col for col in df if col.startswith(pattern)]].columns)
    elif loc == 'end':
        return list(df[[col for col in df if col.endswith(pattern)]].columns)


def get_corr(df, col, method = 'pearson'):
    df = df.corr(method=method)[col].sort_values(ascending=False).reset_index()[1:]
    df = df[~df[col].isnull()]
    return df


def make_comparison(df, title, csv_path, png_path, p_val_thr=0.05, lf_thr=1, var=None):
    df_volcano = make_stats_paired_comparisons(df, var)
    vis_volcano(df_volcano, title, csv_path, png_path, logfold_threshold=lf_thr, p_val_threshold=p_val_thr)
    return df_volcano.sort_values(by='log2FC').reset_index(drop=True)


def make_stats_paired_comparisons(df, var=None):
    stats_dict = defaultdict(dict)
    for m in get_columns(df, pattern='IF', loc='start'):
        if var != None:
            case = df.loc[df[var] == 1][m].dropna().values
            control = df.loc[df[var] == 0][m].dropna().values
        elif var == None:
            m = m[0:-2]
            case = df[f'{m}_2'].dropna().values
            control = df[f'{m}_1'].dropna().values
        stat, p = mannwhitneyu(case, control)
        stats_dict[m]['stat'] = stat
        stats_dict[m]['p_value'] = p
        stats_dict[m]['-log10(p_value)'] = -1*(log10(p))
        stats_dict[m]['log2FC'] = log2(case.mean()) - log2(control.mean())
        stats_dict[m]['n_1'] = len(case)
        stats_dict[m]['n_0'] = len(control)
    df_com = pd.DataFrame(stats_dict).T.sort_values(by='p_value').reset_index()
    _, pvals_corrected, _, _ = fdr(df_com['p_value'].values, alpha=0.05,
                                        method='fdr_bh', is_sorted=False, returnsorted=False)
    df_com.insert(loc=3, column='p_value_nofdrcorrection', value=df_com['p_value'].values)
    df_com['p_value'] = pvals_corrected
    df_com['-log10(p_value)'] = df_com['p_value'].apply(lambda x: -log10(x))
    return df_com


def mwu_compare(case, control, col):
    case = case[col].dropna().values
    control = control[col].dropna().values
    _, p = mannwhitneyu(case, control)
    return p 


def process_patients(path):
    df = pd.read_csv(path, decimal=",")
    df = df.replace('-', np.nan)
    df['I:patient'] = df['I:patient'].astype(int)
    df = df.drop('r:irae_appear', axis = 1)
    df = df.loc[df['I:patient'] != 16]
    df = df.loc[df['I:patient'] != 82]
    df = df.loc[df['I:patient'] != 83]

    controls =  df.iloc[-4:].dropna(axis='columns')
    controls = controls.iloc[:,1:].astype(float)

    # df = df.loc[df['I:patient'] != 101]
    # df = df.loc[df['I:patient'] != 102]
    # df = df.loc[df['I:patient'] != 103]
    # df = df.loc[df['I:patient'] != 104]
    df = df[get_columns(df, 'I:', loc='start') + # Patient identifier
            get_columns(df, 'b:', loc='start') + # Biochemistry
            get_columns(df, 'c:', loc='start') + # Cancer
            get_columns(df, 'i:', loc='start') + # Immunology
            get_columns(df, 'r:', loc='start') + # Response and treatment
            get_columns(df, 's:', loc='start') + # Sequencing and microscopy
            get_columns(df, 'v:', loc='start') + # Clinical variables
            get_columns(df, 'IF:', loc='start')] # Immunophenotypic marker
    df = df.astype(float)
    return df, controls


def scoring_metrics():
    '''
    Scoring metrics to add in multi scoring option of GridSearchCV / RandomizedSearchCV.

    Returns
    ----------
    scoring: Dict
        Dictionary to gives as an argument of multiscoring tasks.
    '''
    scoring = {
        'Accuracy': 'accuracy',
        'AUC': 'roc_auc',
        'Average Precision Score': 'average_precision',
        'Balanced Accuracy': 'balanced_accuracy',
        'F1 Score': 'f1',
        'Log Loss': 'neg_log_loss',
        'Precision': 'precision',
        'Recall': 'recall'
        }
    return scoring


def vis_volcano(df, title, csv_path, png_path, logfold_threshold, p_val_threshold):
    df.to_csv(f'{csv_path}', index=None)
    visuz.volcano(
        title=title,
        path=png_path,
        table=f'{csv_path}',
        lfc="log2FC",
        pv="p_value",
        geneid="index",
        valpha=0.5,
        lfc_thr=logfold_threshold,
        pv_thr=p_val_threshold,
        genenames=tuple(
            df.loc[
                (abs(df['log2FC']) > logfold_threshold) &
                (df['p_value'] < p_val_threshold)
            ]['index']),
        gfont=8)